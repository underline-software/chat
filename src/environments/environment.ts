// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD8FhjgZVMAMbZiPMvwiY1J-ETQvJm3Bio',
    authDomain: 'chat-8cb75.firebaseapp.com',
    databaseURL: 'https://chat-8cb75.firebaseio.com',
    projectId: 'chat-8cb75',
    storageBucket: 'chat-8cb75.appspot.com',
    messagingSenderId: '876635916408'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
