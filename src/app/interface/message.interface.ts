export interface Message {
  name: string;
  message: string;
  dateTime?: number;
  uid?: string;
}
