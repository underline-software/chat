import {Routes} from '@angular/router';
import {ChatroomComponent} from './components/chatroom/chatroom.component';
import {LoginComponent} from './components/login/login.component';


export const appRoutes: Routes = [
  {path: 'chat', component: ChatroomComponent},
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
];
