// Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { appRoutes } from './route';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
// Firebase
import {AngularFirestore, AngularFirestoreModule} from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import {AngularFireAuthModule} from 'angularfire2/auth';
// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ChatroomComponent } from './components/chatroom/chatroom.component';
import { ChatmessageComponent } from './components/chatmessage/chatmessage.component';
import { ChatformComponent } from './components/chatform/chatform.component';
import { UserlistComponent } from './components/userlist/userlist.component';
import { UserComponent } from './components/user/user.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MessageslistComponent } from './components/messageslist/messageslist.component';
// Services
import { UtilityService } from './service/utility.service';
import {ChatService} from './service/chat.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatroomComponent,
    ChatmessageComponent,
    ChatformComponent,
    UserlistComponent,
    UserComponent,
    NavbarComponent,
    MessageslistComponent
  ],
  providers: [UtilityService, ChatService],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebase)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
