import { Component } from '@angular/core';
import {ChatService} from './service/chat.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public login: ChatService
  , public router: Router) {}

  loginEnding() {
    this.login.logout().then( () => {
      this.login.statusLogin().subscribe( (user) => {
        console.log('loginEnding:', user);
        if ( !user ) {
          this.router.navigate(['login']);
        }
      });
    });
  }
}
