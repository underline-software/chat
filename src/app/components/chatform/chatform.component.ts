import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../service/chat.service';

@Component({
  selector: 'app-chatform',
  templateUrl: './chatform.component.html',
  styleUrls: ['./chatform.component.css']
})
export class ChatformComponent implements OnInit {
  message;
  constructor(private chatService: ChatService) { }
  ngOnInit() {}

  handle(event) {
    if (event.keyCode === 13) {
      this.send();
    }
  }
  send() {
    if (this.message.length === 0) {
      return 0;
    }
    this.chatService.sendMessage(this.message).then(
      (succ) => {
        this.message = '';
      }
    ).catch((err) => {
      console.log(err);
    });
  }
}
