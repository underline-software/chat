import {Component, Input, OnInit} from '@angular/core';
import {ChatService} from '../../service/chat.service';

@Component({
  selector: 'app-chatmessage',
  templateUrl: './chatmessage.component.html',
  styleUrls: ['./chatmessage.component.css']
})
export class ChatmessageComponent implements OnInit {
  @Input() message = {};

  constructor(public login: ChatService) {}

  ngOnInit() {
  }

}
