import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../service/chat.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: ChatService
  , private router: Router) {}

  ngOnInit() {}

  login() {
    this.loginService.login();
  }
}
