import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../service/chat.service';

@Component({
  selector: 'app-messageslist',
  templateUrl: './messageslist.component.html',
  styleUrls: ['./messageslist.component.css']
})
export class MessageslistComponent implements OnInit, AfterViewChecked {

  @ViewChild('appmensajes') elementScroll: ElementRef;

  constructor(private chat: ChatService) {}

  ngOnInit() {
    this.chat.getMessage().subscribe();
  }

  ngAfterViewChecked() {
    this.elementScroll.nativeElement.scrollTop = this.elementScroll.nativeElement.scrollHeight;
  }
}
