import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ChatService} from '../../service/chat.service';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.css']
})
export class ChatroomComponent implements OnInit {

  constructor(private router: Router, private login: ChatService) {
    this.login.statusLogin().subscribe( (user) => {
      if ( !user ) {
        this.router.navigate(['login']);
      }
    });
  }
  ngOnInit() {}
}
