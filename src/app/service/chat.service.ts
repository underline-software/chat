import { Injectable } from '@angular/core';
import {Message} from '../interface/message.interface';
import {UtilityService} from './utility.service';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {map} from 'rxjs/operators';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private messageItems: AngularFirestoreCollection<Message>;
  public messages: Message[] = [];
  public usuario: any = {};

  constructor(private database: AngularFirestore
  , private Utility: UtilityService
  , private auth: AngularFireAuth
  , private router: Router) {
    this.auth.authState.subscribe( user => {
      if ( !user ) {
        this.usuario.uid = false;
       return;
      }
      this.usuario.nombre = user.displayName;
      this.usuario.uid = user.uid;
      router.navigate(['chat']);
    });
  }



  sendMessage(msg: string) {
    const mensaje: Message = {
      name: this.usuario.nombre,
      message: msg,
      dateTime: new Date().getTime(),
      uid: this.usuario.uid,
    };

    return this.messageItems.add( mensaje );
  }

  getMessage() {
    this.messageItems = this.database.collection<Message>('chat',
      ref => ref.orderBy('dateTime', 'desc').limit(5));
    return this.messageItems.valueChanges().pipe(map( (mensajes: Message[]) => {
      this.messages = mensajes;
      this.messages.reverse();
    }));
  }

  login() {
    this.auth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    return this.auth.auth.signOut();
  }

  statusLogin() {
    return this.auth.authState;
  }
}
